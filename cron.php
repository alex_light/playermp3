<?php
require_once (dirname(__FILE__).'/vendor/autoload.php');
require_once (dirname(__FILE__).'/class.php');

foreach (glob(dirname(__FILE__).'/mp3/*.mp3') as $file) {
    $file_info = pathinfo($file);
    AnalizeMP3File::CreateImageFromAudioFile($file_info['basename']);
}
function pre($d){
    echo '<pre>';
    print_r($d);
    echo '</pre>';
}