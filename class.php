<?php

Class AnalizeMP3File{
    public static function CreateImageFromAudioFile($Path){

        $mime['image/png'] = 'png';
        $mime['image/jpeg'] = 'jpeg';
        $getID3 = new getID3;
        $OldThisFileInfo = $getID3->analyze(dirname(__FILE__)."/mp3/".$Path);

        if(isset($OldThisFileInfo['comments']['picture'][0])) {
            $newImgNameFile = str_replace('.mp3', "." . $mime[$OldThisFileInfo['comments']['picture'][0]['image_mime']], $Path);
            file_put_contents(dirname(__FILE__) . "/mp3/images/" . $newImgNameFile, $OldThisFileInfo['comments']['picture'][0]['data']);
        }
    }
    public static function getJsonMP3Data(){
        foreach (glob(dirname(__FILE__).'/mp3/*.mp3') as $file) {
            $file_info = pathinfo($file);

            /*title:"Cro Magnon Man",
                    artist:"The Stark Palace",
                    mp3:"http://www.jplayer.org/audio/mp3/TSP-01-Cro_magnon_man.mp3",
                    oga:"http://www.jplayer.org/audio/ogg/TSP-01-Cro_magnon_man.ogg",
                    poster: "http://www.jplayer.org/audio/poster/The_Stark_Palace_640x360.png"*/
            if(file_exists(dirname(__FILE__).'/mp3/images/'.$file_info['filename'].'.png')){
                $imageFile='mp3/images/'.$file_info['filename'].'.png';
            }elseif(file_exists(dirname(__FILE__).'/mp3/images/'.$file_info['filename'].'.jpeg')){
                $imageFile='mp3/images/'.$file_info['filename'].'.jpeg';
            }else{
                $imageFile = '';
            }
            $out[] = array(
                'title'=>$file_info['filename'],
                'mp3'=>'mp3/'.$file_info['basename'],
                'poster'=> $imageFile
            );
        }
        //return $out;
        return json_encode($out);

    }
}