<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3f69ccc313d50a8dfea66126d31f15b9
{
    public static $classMap = array (
        'getID3' => __DIR__ . '/../..' . '/getid3/getid3.php',
        'getid3_exception' => __DIR__ . '/../..' . '/getid3/getid3.php',
        'getid3_handler' => __DIR__ . '/../..' . '/getid3/getid3.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInit3f69ccc313d50a8dfea66126d31f15b9::$classMap;

        }, null, ClassLoader::class);
    }
}
